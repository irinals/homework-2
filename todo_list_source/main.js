// массив активных задач
var data = (localStorage.getItem('todoList'))
  ? JSON.parse(localStorage.getItem('todoList'))
  : [];
// массив завершённых задач
var dataCompleted = (localStorage.getItem('completed'))
  ? JSON.parse(localStorage.getItem('completed'))
  : [];
  
// синхронизация localStorage и Массива JS
function storageSync() {
  localStorage.setItem('todoList', JSON.stringify(data));
  localStorage.setItem('completed', JSON.stringify(data));
}

// рисуем начальный список
renderTodoList();
renderCompletedList();

// Добавить новый элемент в список активных задач
document.getElementById('btn-create').addEventListener('click', function () {
  var value = document.getElementById('input-create').value;
  if (value) {
    addItemToDOM(value);
    document.getElementById('input-create').value = '';

    data.push(value);  
  } 
// добавить новую задачу в localStorage
  
  localStorage.setItem('todoList', JSON.stringify(data));
});

function renderTodoList() {
  if (!data.length) return;

  for (var i = 0; i < data.length; i++) {
    var value = data[i];
    addItemToDOM(value);
  }
}

function addItemToDOM(text) {
  var list = document.getElementById('todo-list');

  var item = document.createElement('li'); 
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
    <span class="secondary-content">
      <div class="item-btn item-btn-del btn-floating btn-small waves-effect waves-light red">x</div>
    </span>
  </div>`;
  item.innerHTML = listItemView;

  

  // добавим слушатель для удаления
  var buttonDelete = item.getElementsByClassName('item-btn-del')[0];
  buttonDelete.addEventListener('click', removeItem);

  list.appendChild(item);
}
// удаление задачи из активных с переносом в завершённые
function removeItem(e) {
  var item = this.parentNode.parentNode.parentNode;
  var list = item.parentNode;
  var value = item.getElementsByClassName('item-text')[0].innerText;

  // удаление задачи из localStorage
  var localData = JSON.parse(localStorage.getItem('todoList'));
  localData.splice(data.indexOf(value), 1);
  localStorage.setItem('todoList', JSON.stringify(localData));

  
  data.splice(data.indexOf(value), 1);
  list.removeChild(item);


// добавить завершённую задачу в список завершённых

  if (value) {
    addCompletedItemToDOM(value);
    dataCompleted.push(value);  
  }

// добавить завершённую задачу в localStorage  
  localStorage.setItem('completed', JSON.stringify(dataCompleted));
};

function renderCompletedList() {
  if (!dataCompleted.length) return;

  for (var i = 0; i < dataCompleted.length; i++) {
    var value = dataCompleted[i];
    addCompletedItemToDOM(value);
  }
}

function addCompletedItemToDOM(text) {
  var list = document.getElementById('completed');
  var item = document.createElement('li'); 
  item.classList = 'collection-item';
  var listItemView = `
  <div class="item">
    <span class="item-text">${text}</span>
  </div>`;
  item.innerHTML = listItemView; 
  list.appendChild(item);
}
